package main

import (
	"decks"
	"fmt"
)

func main() {

	fmt.Println("Initializing Deck.....")
	d := decks.CreateDeck()

	d.Print()

	fmt.Println("Creating a hand.....")
	hand, remaining := d.Hand(5)

	fmt.Println("Hand of deck.....")
	hand.Print()

	fmt.Println("Remaining Deck.....")
	remaining.Print()

	fmt.Println("saving original deck to file....")
	decks.SaveToFile(d, "new_file.txt")

	fmt.Println("Reading from file after saving....")
	decks.ReadFromFile("new_file.txt").Print()
}
