package decks

import (
	"os"
	"testing"
)

func TestCreateDeck(t *testing.T) {
	d := CreateDeck()

	if len(d) != 12 {
		t.Fatalf("wanted 16 but got %v", len(d))
	}
}

func TestSaveToFile(t *testing.T) {
	fileName := "test_file.txt"

	d := CreateDeck()

	SaveToFile(d, fileName)

	data, err := os.ReadFile(fileName)

	if len(data) == 0 || err != nil {
		t.Fatalf("failed to save to file")
	}

	os.Remove(fileName)
}
