package decks

import (
	"fmt"
	"os"
	"strings"
)

type deck []string

func CreateDeck() deck {
	a := []string{"Ace", "Diamonds", "Heart"}

	b := []int{1, 2, 3, 4}

	var cards deck
	for _, i := range a {
		for _, j := range b {
			cards = append(cards, fmt.Sprint(j)+" of "+i)
		}
	}
	return cards
}

func (d deck) Hand(size int) (deck, deck) {
	return d[0:size], d[size:]
}

func (d deck) Print() {
	for _, deck := range d {
		fmt.Println(deck)
	}
}

func SaveToFile(d deck, filename string) error {
	s := strings.Join(d, ", ")
	return os.WriteFile(filename, []byte(s), 0644)
}

func ReadFromFile(filename string) deck {
	data, error := os.ReadFile(filename)

	if error != nil {
		os.Exit(1)
	}
	s := string(data)
	d := strings.Split(s, ", ")
	return d
}
